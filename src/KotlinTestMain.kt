/**
 * Created by looser43 on 5/26/17.
 */

fun main (arg: Array<String>) {
    println("Hellow World")

    val a = "Saadat"
    println(a)

    val range0to10 = 0..10
    for(x in range0to10) println("Position[$x] : ${x+1}")

    // ----- VARIABLES -----

    // Create a read only variable
    //val name = "Sayem"

    // Mutable (changeable) variable
    //var myAge = 25

    // Kotlin uses type inference, but you can define the type

    var bigInt: Int = Int.MAX_VALUE
    var smallInt: Int = Int.MIN_VALUE

    println("Biggest Int : " + bigInt)
    println("Smallest Int : " + smallInt)

    var bigLong: Long = Long.MAX_VALUE
    var smallLong: Long = Long.MIN_VALUE

    println("Biggest Long : " + bigLong)
    println("Smallest Long : " + smallLong)

    var bigDouble: Double = Double.MAX_VALUE
    var smallDouble: Double = Double.MIN_VALUE

    println("Biggest Double : " + bigDouble)
    println("Smallest Double : " + smallDouble)

    var bigFloat: Float = Float.MAX_VALUE
    var smallFloat: Float = Float.MIN_VALUE

    println("Biggest Float : " + bigFloat)
    println("Smallest Float : " + smallFloat)

    // Doubles are normally precise to 15 digits
    var dblNum1: Double = 1.11111111111111111
    var dblNum2: Double = 1.11111111111111111

    println("Sum : " + (dblNum1 + dblNum2))


}